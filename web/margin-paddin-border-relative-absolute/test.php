<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title></title>
  <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <script type="text/javascript">
    
    $(function () {
      $('#box').click(function () {
          $('#absol').css({
              backgroundColor: 'red'
          })
      })
    })
  </script>
  <style type="text/css">
    #box {
      border: 1px black solid;
      padding: 12px;
      margin: 50px;
      background-color: blue;
    }
    #second {
      border-bottom: 1px solid blue;
      border-top: 2px dotted green;
      border-left: 5px dashed yellow;
      /*border-left-style: dotted;*/    /* typ/styl lewego obramowania */
      border-style: solid; /* TYLKO styl obramowania : bez koloru, bez grubości */
      
      border-bottom-style: dotted; /* ta reguła ostatecznie nadpisała */
      
    }
    
    /*z innej beczki o relative/absolute*/
    #absol {
      position: relative;
    }
    #inner {
      position: absolute;
      top: 10px;
      left: 20px;
    }
    .one {
      border: 1px solid black;
    }
  </style>
</head>
<body>
  <div id="box">
    text
  </div>
  <div id="second">
    rozdzielenie
  </div>
  <h1>relative apsolute</h1>
  <hr />
  <div id="absol" class="one">
    <p>relative</p>
    <div class="one" id="inner">wewnętrzny</div>
  </div>
  
  <hr />
  <ul id="main">
    <li>
      <ul>
        <li>jeden</li>
        <li>dwa</li>
        <li>trzy</li>
        <li>cztery</li>
      </ul>
    </li>
    <li>
      <ul>
        <li>jeden</li>
        <li>dwa</li>
        <li>trzy</li>
        <li>cztery</li>
      </ul>
    </li>
    <li>
      <ul>
        <li>jeden</li>
        <li>dwa</li>
        <li>trzy</li>
        <li>cztery</li>
      </ul>
    </li>
  </ul>
</body>
</html>