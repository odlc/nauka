if(typeof console === "undefined")   console = { log: function() { },dir: function(){} };
/**
 * Klasa do przechowywania phpsessid
 *
 * @example Ses.GetSessid() - zwraca wartość sessid jeśli nie ma w ciastku to wewnętrzną wartość
 * @example Ses.DestroySessid() - niszczy sessid z ciastka jak i z wewnątrz z klasy
 *
 * WSKAZÓWKA - można $.cookie('PHPSESSID') zastąpić '<?php echo session_name(); ?>'
 *
 * @name CookieClass()
 * @author Szymon Działowski/grytysek@gmail.com
 */
function CookieClass() {
  this.phpsess = ($.cookie('<?php echo session_name(); ?>')===null) ? '' : $.cookie('<?php echo session_name(); ?>') ;
  this.GetSessid = function() {
    var temp  = $.cookie('<?php echo session_name(); ?>');
    if (temp != null) {
      this.phpsess = temp;
      return temp;
    }
    return this.phpsess;
  }
  this.DestroySessid = function() {
    $.cookie('<?php echo session_name(); ?>',null);
    this.phpsess = '';
  }
}
function unloader(target) {
  $(target).children('.myAjaxLoading').remove();
  $(target).children().show();
  $(target+' .pageStart').hide();
  $(target+' .pageStop').hide();
}
function preloader(target) {
    var h = $(target).height();
    var w = $(target).width();
    var img = '';
    if( $(target).height() > 33) img = 'medium.gif';
    else                         img = 'small.gif';
    $(target).children().hide();
    $(target).append('<table class="myAjaxLoading" height="'+h+'" width="'+w+'" style="text-align: center;" valign="middle"><tbody><tr><td><img  src="./img/'+img+'"/></td></tr></tbody></table>');
}
function getJava(targetFile,dane)  { // pobranie z serwera javascript i wykonanie getConfirm
  $.ajaxSetup({
    data: { // albo tak: //data: "name=John&location=Boston",
      '<?php echo session_name(); ?>': Ses.GetSessid()     // dobrze jest też ciastko wysłać postem za każdym razem
    },
    beforeSend: function(){
      SET.l++;
      if (SET.aed !== null) $('#'+SET.aed).show();
    }
  }); // aby nie wywoływało funkcji preloader bo teraz wywali błąd
  $.ajax({
    type: "POST",
    url: targetFile,
    data: dane,
    error: function(){
      SET.l--;
      if (SET.aed !== null){
        var temp = $('#'+SET.aed+' span').html();
        $('#'+SET.aed+' span').html('Błąd...');
        setTimeout(function(){
          if (SET.l == 0) $('#'+SET.aed).hide();
          $('#'+SET.aed+' span').html(temp);
        },SET.errSpeed);
      }
    },
    success: function(back){
      SET.l--;
      if (SET.aed !== null && SET.l == 0) $('#'+SET.aed).hide();
      eval(back);
    }
  });
}

function checkLink() {
  this.storage = {};
/*this.storage = {
    '#info':{
      t:target,
      f:targetFile,
      p:dane
    },
    '#login':{
      t:target,
      f:targetFile,
      p:dane
    }
  };  */
  this.check = function (v) {
    try       { this.storage = $.getToObj(v);}
    catch (e) { return false;} return true;
  }
  this.getCurrentHash = function() {
    var r = window.location.href;
    var i = r.indexOf("#");
    return (i >= 0 ? r.substr(i+1) : "");
  }
  this.runBookmark = function(arg0){
    //alert('opera in functon');
    if (this.check(this.getCurrentHash())) {
    //console.dir(this.storage);
      //alert('wewnątrz opera');
      var tt = this.storage;
      for (var t in tt) {
        //alert('tt');
        if (t == '#'+SET.centerDiv) getData(tt[t].tf,tt[t].d,t,'hist'); // jeśli centralny to zapisz w historii
        else                        getData(tt[t].tf,tt[t].d,t);
      }
    }
    else arg0();
  }
}
var runBook = new checkLink();

/**
  * jeśli podamy czwarty argument wtedy włączymy obsługę historii dla tego zapytania ajax
  * podajemy wtedy obiekt z wpisanymi divami jakie chcemy zapisać w historii np:
  * {
  *   'info': '',
  *   'center': ''
  * }
  * lub string 'def' lub cokolwiek innego // oznacza że mają być zapisane do historii divy domyślne zapisane w obiekcie globalnym SET.hd
  * , odwiązanie zdarzeń i komponentów od wskazanego kontenera
  * oraz dowiązanie wykonanie javascript po dobraniu nowej zawartości kontenera
  */
function getData(targetFile,dane,target) {  // pobranie do div asynchronicznie zawartości / format data: {name:'John',location: 'Boston','choices[]': ["Jo&n", "Su=sa&n"]}
  $.ajaxSetup({
    data: { // albo tak: //data: "name=John&location=Boston",
      '<?php echo session_name(); ?>': Ses.GetSessid()     // dobrze jest też ciastko wysłać postem za każdym razem
    }
  });
  var arg = arguments;
  $.ajax({
    type: "POST",
    url: targetFile,
    data: dane,
    context: $(target),
    beforeSend: function(){
      SET.l++;
      if (SET.aed !== null) $('#'+SET.aed).show();
      else                  preloader(target);
    },
    error: function(){ // jeśli wystąpi błąd to loader zostaje usunięty a poprzednia zawartość przywrócona
      SET.l--;
      if (SET.aed !== null){
        var temp = $('#'+SET.aed+' span').html();
        $('#'+SET.aed+' span').html('Błąd...');
        setTimeout(function(){
          if (SET.l == 0) $('#'+SET.aed).hide();
          $('#'+SET.aed+' span').html(temp);
        },SET.errSpeed);
      }
      else {
        $(target).children().find('img').attr('src','./img/error.jpg');
        setTimeout(function() {
          unloader(target);
        }, SET.errSpeed);
      }
    },
    success: function(back){

      SET.l--;
      if (SET.aed !== null && SET.l == 0)                 $('#'+SET.aed).hide();
      $(this).animate({ opacity: ['toggle', 'swing'],height: ['toggle', 'swing']},SET.fxSpeed,function(){

        eval($(target+' .pageStop').html()); // wykonanie skryptów odwiązujących drzewka, tooltipy, komponenty i zdarzenia
        $(this).html(back);
        $(this).animate({ opacity: ['toggle', 'swing'],height: ['toggle', 'swing']},SET.fxSpeed,function(){
          // UWAGA  - nie mylić - strona OBECNA [AKTUALNIE DOSTARCZONA] jest archiwizowana
          // jeśli podaliśmy czwarty argument z divami to zapisujemy je do hostorii

          SET.historyBuffer = {
              tf:targetFile,
              d:dane,
              t:target
          };

          runBook.storage[target] = { //dopisanie do stanu przeglądarki
              tf:targetFile,
              d:dane
          };
          //console.log('runBook:');
          console.log('- Bookmark - dodano do konstruktora -');
          console.dir(runBook.storage);
          //console.dir(SET.historyBuffer);
          if (typeof arg[3] == 'string')   {// def - domyślne argumenty z globalnego obiektu SET.hd
            //console.log('string');
            addHistoryEvent(SET.historyBuffer); // domyślnie
          }
          else if (typeof arg[3] == 'object') {// a jeśli obiekt to go podajemy
            addHistoryEvent(SET.historyBuffer,arg[3]);  // ręcznie
            //console.log('object');
          }
          else {
            console.log('- getData - bez zapisu do historii -');
          }
          eval($(target+' .pageStart').html()); // wykonanie skryptów startowych dla danej zawartości
        });
      });
    }
  });
}