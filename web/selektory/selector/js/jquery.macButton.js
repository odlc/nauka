/**
* Wykonał Szymon Działowski
* wystarczy tylko dodać do buttona klasę 'macBlue' lub 'macWhite' - [macWhite tylko dla elementu <button></button>]
*/
(function($) {
  $.fn.unMacButton = function(options) {
    options = options || {};
    var defaultOpt = {
      checkboxCls      : options.checkboxCls      || 'ez-checkbox'       ,
      checkboxHoverCls : options.checkboxHoverCls || 'ez-checkbox-hover' ,
      checkedCls       : options.checkedCls       || 'ez-checked'        ,
      checkedHoverCls  : options.checkedHoverCls  || 'ez-checked-hover'  ,

      radioCls         : options.radioCls         || 'ez-radio'          ,
      radioHoverCls    : options.radioHoverCls    || 'ez-radio-hover'    ,
      selectedCls      : options.selectedCls      || 'ez-selected'       ,
      selectedHoverCls : options.selectedHoverCls || 'ez-selected-hover' ,
      hideCls     : 'ez-hide'
    };
    return this.each(function() {
      var $this = $(this);
      if ($this.hasClass('inputMac')) { // inputy
        $this.removeClass('inputMac').addClass('macBlue').unwrap().next().remove();
        $this.prev('.macLeft').remove();
        $this.unwrap().removeClass('macBlueOn');
        $this.unbind();
      }
      else if ( $this.attr('type') == 'checkbox' && $this.hasClass(defaultOpt.hideCls)) {
        $this.addClass('macBlue').removeClass(defaultOpt.hideCls).removeClass('macBlueOn').unwrap().unbind();
      }
      else if ( $this.attr('type') == 'radio' && $this.hasClass(defaultOpt.hideCls)) {
        $this.addClass('macBlue').removeClass('macBlueOn').removeClass(defaultOpt.hideCls).unwrap().unbind();
      }
      else if ($this.hasClass('macBlueOn') || $this.hasClass('macWhiteOn')) {    // buttony
        if ($this.hasClass('macBlueOn'))  $this.removeClass('macBlueOn').addClass('macBlue');
        if ($this.hasClass('macWhiteOn')) $this.removeClass('macWhiteOn').addClass('macWhite');
        $this.html($this.children('.macCenter').html()).unbind();
      }
    });

  }
  $.fn.macButton = function(options) {
    options = options || {};
    var defaultOpt = {
      checkboxCls      : options.checkboxCls      || 'ez-checkbox'       ,
      checkboxHoverCls : options.checkboxHoverCls || 'ez-checkbox-hover' ,
      checkedCls       : options.checkedCls       || 'ez-checked'        ,
      checkedHoverCls  : options.checkedHoverCls  || 'ez-checked-hover'  ,

      radioCls         : options.radioCls         || 'ez-radio'          ,
      radioHoverCls    : options.radioHoverCls    || 'ez-radio-hover'    ,
      selectedCls      : options.selectedCls      || 'ez-selected'       ,
      selectedHoverCls : options.selectedHoverCls || 'ez-selected-hover' ,
      hideCls     : 'ez-hide'
    };
    return this.each(function() {
      var $this = $(this);
      if (($(this).attr('type') == 'text' || $(this).attr('type') == 'password') && $this.hasClass('macBlue')) { // inputy
        $this.removeClass('macBlue').addClass('inputMac').wrap('<div class="macCenter"></div>')
          .parent()
          .before('<div class="macLeft" ></div>')
          .after( '<div class="macRight"></div>');
        $this.parent()
          .add($this.parent().prev())
          .add($this.parent().next())
          .wrapAll('<span class="macInput"></span>');
        $this.removeClass('macInput');

        $this.focus(function(){
          $(this).parent().addClass('macCenterFocus').removeClass('macCenter');
          $(this).parent().prev('div.macLeft').addClass('macLeftFocus').removeClass('macLeft');
          $(this).parent().next('div.macRight').addClass('macRightFocus').removeClass('macRight');
        }).blur(function(){
          $(this).parent().removeClass('macCenterFocus').addClass('macCenter');
          $(this).parent().prev('div.macLeftFocus').addClass('macLeft').removeClass('macLeftFocus');
          $(this).parent().next('div.macRightFocus').addClass('macRight').removeClass('macRightFocus');
        });





          /*
            <span class="macInput">
              <div class="macLeft"></div>
              <div class="macCenter"><input type="text" value="input macInput"/></div>
              <div class="macRight"></div>
            </span>
        */
      }
      else if ( $this.attr('type') == 'checkbox' && $this.hasClass('macBlue')) {
              $this.removeClass('macBlue').addClass('macBlueOn').addClass(defaultOpt.hideCls).wrap('<div class="'+defaultOpt.checkboxCls+'">').change(function() {
                if ($this.is(':checked')) $this.parent().removeClass(defaultOpt.checkboxHoverCls).addClass(defaultOpt.checkedHoverCls);
                else                      $this.parent().removeClass(defaultOpt.checkedHoverCls).addClass(defaultOpt.checkboxHoverCls);

              });
              $this.hover(
                function(){
                  if ($this.is(':checked'))   {$this.parent().addClass(defaultOpt.checkedHoverCls).removeClass(defaultOpt.checkedCls);}
                  else                        {$this.parent().addClass(defaultOpt.checkboxHoverCls);}
                },
                function(){
                  if ($this.is(':checked'))   {$this.parent().addClass(defaultOpt.checkedCls).removeClass(defaultOpt.checkedHoverCls);}
                  else                        {$this.parent().removeClass(defaultOpt.checkboxHoverCls);}
                }
              );
              if( $this.is(':checked') ) $this.parent().addClass(defaultOpt.checkedCls);
      }
      else if ( $this.attr('type') == 'radio' && $this.hasClass('macBlue')) {
              $this.removeClass('macBlue').addClass(defaultOpt.hideCls).addClass('macBlueOn').wrap('<div class="'+defaultOpt.radioCls+'">').change(function() {
                $('input[name="'+$(this).attr('name')+'"]').each(function() {
                  $(this).parent().removeClass(defaultOpt.selectedCls);
                  if ($(this).is(':checked')) $this.parent().removeClass(defaultOpt.radioHoverCls).addClass(defaultOpt.selectedHoverCls);
                });
              });
              $this.hover(
                function(){
                  if ($this.is(':checked'))   {$this.parent().addClass(defaultOpt.selectedHoverCls).removeClass(defaultOpt.selectedCls);}
                  else                        {$this.parent().addClass(defaultOpt.radioHoverCls);}
                },
                function(){
                  if ($this.is(':checked'))   {$this.parent().addClass(defaultOpt.selectedCls).removeClass(defaultOpt.selectedHoverCls);}
                  else                        {$this.parent().removeClass(defaultOpt.radioHoverCls);}
                }
              );
              if( $this.is(':checked') ) {
                $this.parent().addClass(defaultOpt.selectedCls);
              }
      }
      else if ($this.hasClass('macBlue') || $this.hasClass('macWhite')) {    // buttony
        if ($this.hasClass('macBlue')) $this.removeClass('macBlue').addClass('macBlueOn');
        if ($this.hasClass('macWhite')) $this.removeClass('macWhite').addClass('macWhiteOn');
        $this.html(''+
          '<div class="macLeft"></div>'+
          '<div class="macCenter">'+$this.html()+'</div>'+
          '<div class="macRight"></div>'+
        '');
        var  widthOfButton = 1;
        while ($this.height() > 30)  $this.width(widthOfButton += 3);   //tylko dla mac-big
        /*
        while ($(this).height() > 19)  $(this).width(widthOfButton += 3); // inne skórki
        if ($.browser.msie) {                                             // inne skórki
          $(this).width(widthOfButton+20);                                // inne skórki
          $(this).children('.mac-blur-center').width(widthOfButton);      // inne skórki
        }                                                                 // inne skórki
        */
      }
    });
  }
})(jQuery);
/*
$(document).ready(function() {
  $('button.macBlueOn, button.macWhite, input.macInput, input').livequery(function(){
    $(this).macButton();
  });
});
*/