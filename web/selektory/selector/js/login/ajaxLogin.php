<?php ob_start(); 
      require_once('../../inc/db.php');
      require_once('../../inc/logClass.php');
      $login = new Ses($SET['tabZUzytk']);// nazwa tabeli z użytkownikami


      $logStatus = '';

      $loginData['login'] = (isset($_GET['login']))  ? $_GET['login'] : false ;
      $loginData['haslo'] = (isset($_GET['haslo']))  ? $_GET['haslo'] : false ;
      $loginData['wyloguj'] = (isset($_GET['wyloguj']))  ? $_GET['wyloguj'] : false ;

      $loginData['login'] = (isset($_POST['login']))  ? $_POST['login'] : $loginData['login'] ;
      $loginData['haslo'] = (isset($_POST['haslo']))  ? $_POST['haslo'] : $loginData['haslo'] ;
      $loginData['wyloguj'] = (isset($_POST['wyloguj']))  ? $_POST['wyloguj'] : $loginData['wyloguj'] ;

      $dumpGlobals = (isset($_POST['dumpGlobals']))  ? $_POST['dumpGlobals'] : false ;



      //ob_get_contents();
      //ob_end_clean();
      //ob_start();

      if ($loginData['wyloguj']) { // wylogowywanie
        $login->LogOff();

          //Ses.DestroySessid(); // bardzo ważne - niszczy po stronie serwera ciastko i sessid w klasie sesyjnej
                               // oraz odwiązuje od następnych zdarzeń jQuery ajax wysyłanie ciastka
        ?>
          Ses.DestroySessid();
          getData('./js/login/loginForm.php',{},'#loginBox')
          getData('./js/login/ajaxLogin.php',{dumpGlobals : '1'},'#'+SET.centerDiv,'hist');
        <?php
      }
      elseif ($dumpGlobals) {
        //ob_end_clean();
        //ob_start();
        $login->CheckAndRespawn();
        echo dumpGlobals($login);
        ?>
          <button onclick="getData('./form/page.php',{page : '1'},'#'+SET.centerDiv,'hist');">strona nr 1</button>
        <?php
      }


      elseif ($loginData['login'] && $loginData['haslo']) {   // logowanie
        if ($login->CheckAndRespawn() || ($login->logIn($loginData['login'],$loginData['haslo']) === true)) {  // musimy porównać (===) bo -1 niestety jest prawdą :/
          ?>
            Ses.phpsess = '<?php echo session_id(); ?>';
            getData('./js/login/loginForm.php',{},'#loginBox')
            $.cookie('<?php echo session_name(); ?>','<?php echo session_id(); ?>',{path: '/',expires: 1/24*<?php echo $SET['cookieTime']; ?>, secure: false});
            getData('./js/login/ajaxLogin.php',{dumpGlobals : '1'},'#'+SET.centerDiv,'hist');
          <?php
        }
        else {
          if ($login->IsLogged() === 0) {
            ?>
              $('#loginErrors').html('Nie mogę połączyć z bazą danych.');
            <?php
          }
          elseif ($login->IsLogged() === -1) {
            ?>
              $('#loginErrors').html('Niepoprawny login lub haslo.');
              $('#login').select();
            <?php
          }
          ?>
            getData('./js/login/ajaxLogin.php',{dumpGlobals : '1'},'#'+SET.y,'hist');
          <?php
        }
      }
      else { // nie udana próba autoryzacji
        ?>
          $('#loginErrors').html('nieudana próba autoryzacji');
          $('#haslo').val('');
          $('#login').val('');
        <?php
      }

      function dumpGlobals($login=false) {
          $d = '';
          $data ='';
          $data .= '<pre>';
          $data .= "\nsess:  ";
          $d .= (isset($_SESSION)) ? print_r($_SESSION,true) : print_r(Array(),true);
          $data .= $d;
          $data .= "\npost:  ";
          $data .= print_r($_POST,true);
          $data .= "\nget:  ";
          $data .= print_r($_GET,true);
          $data .= "\ncoo:  ";
          $data .= print_r($_COOKIE,true);
          $data .= '</pre>';
          if ($login)
            if ($login->IsAdmin()) $data .= 'IsAdmin() - '.$_SESSION['is_admin'];
          return '<input type="text" value="dodatkowy input na sprawdzenie metody live"/><br />'.$data;
      }
      sleep($SET['sleepTime']);
      ob_end_flush();
 ?>