

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{
        "required":{
          "regex":"none",
          "alertText":"* Pole wymagane",
          "alertTextCheckboxMultiple":"* Wybierz opcję",
          "alertTextCheckboxe":"* To pole wyboru jest wymagane"},
        "length":{
          "regex":"none",
          "alertText":"* Od ",
          "alertText2":" do ",
          "alertText3": " znaków wymaganych"},
        "maxCheckbox":{
          "regex":"none",
          "alertText":"* Przekroczona liczba pól wyboru"},
        "minCheckbox":{
          "regex":"none",
          "alertText":"* Wybierz ",
          "alertText2":" opcji(ę)"},
        "confirm":{
          "regex":"none",
          "alertText":"* Pola nie są identyczne"},
        "telephone":{
          "regex":"/^[0-9\-\(\)\ ]+$/",
          "alertText":"* Nieprawidłowy nr telefonu"},
        "email":{
          "regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
          "alertText":"* Nieprawidłowy e-mail"},
        "date":{
          "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
          "alertText":"* Nieprawidłowa data, dozwolony format RRRR-MM-DD"},
        "onlyNumber":{
          "regex":"/^[0-9\ ]+$/",
          "alertText":"* Tylko liczby"},
        "noSpecialCaracters":{
          "regex":"/^[0-9a-zA-Z]+$/",
          "alertText":"* Znaki specjalne niedozwolone"},
        "ajaxUser":{
          "file":"./form/makeUser.php",
          "extraData": "kkk",
          "alertTextOk":"* Nazwa użytkownika dostępna",
          "alertTextLoad":"* Sprawdzanie, proszę czekać",
          "alertText":"* Nazwa użytkownika zajęta"},
        "ajaxName":{
          "file":"validateUser.php",
          "alertText":"* Ta nazwa jest zajęta",
          "alertTextOk":"* Ta nazwa jest dostępna",
          "alertTextLoad":"* Sprawdzanie, proszę czekać"},
        "onlyLetter":{
          "regex":"/^[a-z\ \']+$/i",
          "alertText":"* Tylko litery [Eng]"},
        "almostAll":{
          "regex":"/^[0-9a-ząćęłńóśżźĄĆĘŁŃÓŚŻŹ!@#$\\?%^&*()'-_\\s]+$/i",
          "alertText":"* Możesz używać prawie wszystkie znaki"},
        "onlyLetterPl":{
          "regex":"/^[a-zA-ZąćęłńóśżźĄĆĘŁŃÓŚŻŹ']+$/",
          "alertText":"* Tylko litery [pl]"},
        "onlyLetterPlAndDigit":{
          "regex":"/^[0-9a-zA-ZąćęłńóśżźĄĆĘŁŃÓŚŻŹ']+$/",
          "alertText":"* Tylko litery [pl] i cyfry"},
        "onlyLetterPlSpace":{
          "regex":"/^[a-zA-ZąćęłńóśżźĄĆĘŁŃÓŚŻŹ'\\s]+$/",
          "alertText":"* Tylko litery [pl] i białe znaki"},
        "onlyLetterPlSpaceNumber":{
          "regex":"/^[a-zA-ZąćęłńóśżźĄĆĘŁŃÓŚŻŹ'\\s]+$/",
          "alertText":"* Tylko litery [pl], cyfry i białe znaki"},
        "passLev1":{
          "regex" : "/^(?=.*\\d)(?=.*[a-z]).{1,}$/i",
          "alertText":"* Litera i cyfra"},
        "passLev2":{
          "regex" : "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{0,}$/",
          "alertText":"* Duża litera, mała litera i cyfra"},
        "passLev3":{
          "regex" : "/^(?=.*\d)(?=.*[a-z])(?=.*[^a-zA-Z0-9])(?=.*[A-Z]).{0,}$/",
          "alertText":"* Duża litera, mała litera, cyfra i jakiś znak specjalny"},



        "validate2fields":{
            "nname":"validate2fields",
            "alertText":"* You must have a firstname and a lastname"},
        "imieINazwisko":{
            "nname":"wpiszImieINazwisko",
            "alertText":"* Wpisz imię i nazwisko"},
        "mojeWlasnePoleIValidator" : {
          "nname" : "mojeWlasnePoleIValidatorFunction",
          "alertText" : "* Sprawdzanie według time() mod 2"}
			}
		}
	}
})(jQuery);





$(document).ready(function() {
	$.validationEngineLanguage.newLang()
});