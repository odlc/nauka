<?php



class Validator {  
  var $isAnyErr = true; // true - ok // false - jakiś błąd
  var $pass = Array(
    'level1' => '/^(?=.*\d)(?=.*[a-z]).{0,}$/i',
      //"* Litera, cyfra, i conajmniej 4 znaki ale nie więcej jak 45"
    'level2' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{0,}$/',
      //'* Duża litera, mała litera, cyfra, i conajmniej 4 znaki ale nie więcej jak 4"',
    'level3' => '/^(?=.*\d)(?=.*[a-z])(?=.*[^a-zA-Z0-9<>])(?=.*[A-Z]).{0,}$/',
      //"* Duża litera, mała litera, cyfra, jakiś znak specjalny bez '<>'"
    'level4' => '',
    'level5' => '',
    'level6' => '',
    'level7' => '',
    'level8' => '',
    'level9' => '',
    'level10' => ''
  );
  function reset() {
    $this->isAnyErr = true;
  }
  function isErr() { // czy są jakieś błędy ? // czy $this->isAnyErr == false;
    return ($this->isAnyErr == false);
  }
  function truefalse($tf) {
    global $fp;
    $fp->info('truefalse result: '.$this->btos($tf));
    if($tf)   return true;                  // prawda
    else return $this->isAnyErr = false;    // fałsz
  }
  function checkPreg($preg,$str) {
    global $fp;
    $fp->info('field: \''.$str.'\' pattern php:\''.$preg.'\' result:'.$this->btos(preg_match($preg, $str)));
    if(preg_match($preg, $str))   return true; // pasuje do wyrażenia
    else        return $this->isAnyErr = false;        // NIE pasuje do wyrażenia
  }
  
  
  function almostAll($str) { // '* Tylko litery (eng)'
    global $fp;
    $pattern = '/^[0-9a-ząćęłńóśżźĄĆĘŁŃÓŚŻŹ!@#\$%\^\?&\*\(\)\'\-_\s]+$/i';
    $fp->info('field: \''.$str.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $str)));
    if(preg_match($pattern, $str))   return true; // są tylko litery
    else return $this->isAnyErr = false;                // NIE są tylko litery
  }
  function email($email) {
    global $fp;
    $pattern = '/^[a-z0-9.\-_]+@[a-z0-9\-.]+\.[a-z]{2,4}$/i';
    $fp->info('field: \''.$email.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $email)));
    if(preg_match($pattern, $email))   return true; // prawidłowy
    else return $this->isAnyErr = false;                                                        // NIEprawidłowy
  }
  function oN($str) { // only Number
    global $fp;
    $pattern = '/^[0-9]+$/';
    $fp->info('field: \''.$str.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $str)));
    if(preg_match($pattern, $str))   return true; // są tylko cyfry
    else return $this->isAnyErr = false;                // NIE są tylko cyfry
  }

  function oL($str) { // '* Tylko litery (eng)'
    global $fp;
    $pattern = '/^[a-z]+$/i';
    $fp->info('field: \''.$str.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $str)));
    if(preg_match($pattern, $str))   return true; // są tylko litery
    else return $this->isAnyErr = false;                // NIE są tylko litery
  }
  function oLPl($str) { //'* Tylko litery [pl]'
    global $fp;
    $pattern = "/^[a-ząćęłńóśżźĄĆĘŁŃÓŚŻŹ']+$/i";
    $fp->info('field: \''.$str.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $str)));
    if(preg_match($pattern, $str))   return true;
    else return $this->isAnyErr = false;
  }
  function oLAD($str) { //'* Tylko litery [pl] i cyfry'
    global $fp;
    $pattern = "/^[0-9a-ząćęłńóśżźĄĆĘŁŃÓŚŻŹ']+$/i";
    $fp->info('field: \''.$str.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $str)));
    if(preg_match($pattern, $str))   return true;
    else return $this->isAnyErr = false;
  }
  function oLPlSpace($str) { //'* Tylko litery [pl] i białe znaki'
    global $fp;
    $pattern = "/^[a-ząćęłńóśżźĄĆĘŁŃÓŚŻŹ'\s]+$/i";
    $fp->info('field: \''.$str.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $str)));
    if(preg_match($pattern, $str))   return true;
    else return $this->isAnyErr = false;
  }
  function oLPlSpaceNumber($str) { //'* Tylko litery [pl], cyfry i białe znaki'
    global $fp;
    $pattern = "/^[0-9a-ząćęłńóśżźĄĆĘŁŃÓŚŻŹ'\s]+$/i";
    $fp->info('field: \''.$str.'\' pattern php:\''.$pattern.'\' result:'.$this->btos(preg_match($pattern, $str)));
    if(preg_match($pattern, $str))   return true;
    else return $this->isAnyErr = false;
  }

  function lenght($od,$do,$str) {     // sprawdza czy string ma oczekiwaną długość
    global $fp;
    $fp->info('od: '.$od.' do: '.$do.' string php:\''.$str.'\' result: '.$this->btos((strlen($str) >= $od && strlen($str) <= $do )));
    if (strlen($str) >= $od && strlen($str) <= $do ) return true;  // mieści się w zakresie
    else         return $this->isAnyErr = false;                   // NIE mieści się w zakresie
  }
  function confirm($pole1,$pole2) { // sprawdza czy pola są identyczne
    global $fp;
    $fp->info('compare: \''.$pole1.'\' field2 php:\''.$pole2.'\' result: '.$this->btos($pole1 == $pole2));
    if ($pole1 == $pole2)  return true;   // są identyczne
    else return $this->isAnyErr = false; // NIE są identyczne
  }
  function pass($level,$pass) {
    global $fp;
    $fp->info('password level: \''.$level.'\'field:\''.$pass.'\' pattern php:\''.$this->pass[$level].'\' result:'.$this->btos(preg_match($this->pass[$level], $pass)));
    if(preg_match($this->pass[$level], $pass))   return true; // dobre hasło
    else        return $this->isAnyErr = false;              // NIE dobre hasło
  }
  function btos($bool) { 
    return ($bool) ? 'true' : 'false' ;
  }
}
?>