<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="pl" xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
  <head>
    <title>Wzór</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet"           type="text/css" href="./style/style.css" title="default"/>

    <style type="text/css">
      @charset "utf-8";
      <!--
      body {
        height : 110%;
        font-size: 12px;
      }
      .focus {
        background-color: gray;
        color: yellow;
      }
       .tip{
                position: absolute;
                z-index: 100000;
                border: 1px solid #444444; /*1px solid #FFD5AA;*/
                background: #000; /* #F8FFBF;*/
                color: #fff;  /*#000;*/
                font: bold 100% Arial;
                padding: 5px;
                display: none;
                /*white-space: nowrap;*/
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
                opacity: 0.9;
                filter: alpha(opacity=90);
            }

      -->
    </style>
    <noscript>Twoja przeglądarka nie obsługuje JavaScriptu lub wyłączyłeś jego obsługę.</noscript>

    <link rel="stylesheet" href="./js/tooltip/jquery.tooltip.css" />
    <script language="javascript" type="text/javascript" src="./js/jquery-1.4.2.min.js"></script>
    <script language="javascript" type="text/javascript" src="./js/jquery-backgroundAnimation.js"></script>
    <script language="javascript" type="text/javascript" src="./js/jquery.monnaTip.js.php"></script>
    
    
    <!--
    <script language="javascript" type="text/javascript" src="./js/tooltip/jquery.dimensions.js"></script>
    <script language="javascript" type="text/javascript" src="./js/tooltip/jquery.tooltip.js"></script>
-->
    <script language="javascript" type="text/javascript"  >
      // <!-- <![CDATA[


      $(function(){

        $.ajaxSetup({
          type: "POST",
          cache: false,
          data: { // albo tak: //data: "name=John&location=Boston",
            '<?php echo session_name(); ?>': 'ses'     // dobrze jest też ciastko wysłać postem za każdym razem
          },
          //context: $(target),
          //usernameString: 'simon',
          //passwordString: 'klop',
          //dataType:"json",
          beforeSend: function(){
            preloader(this);
          },
          timeoutNumber: 1000,
          //timeout: 1000, // po jakim czasie zaprzestać oczekiwania i wywołać funkcję error
          error: function(){
            alert('obsługa błedu.');
          },
          success: function(data){
            $(this).html(data);
          },
          complete:   function(html){
            //alert("Żądanie zostało zakończone");
          }
          //xhr : GetXmlHttpObject
        });



          
         $('*[title]').toolTip();
      });
      // ]]> -->
    </script>
  </head>

  <body>

      <div class="lPanelBox">
        <div title="wiadomość">wiadomość</div><br />
        <div title="./txt.php" post="{'zmienna' : 'wartosc'}">ajax</div>
        <div title="./img/bg.jpg"  img="img">obrazek</div>
        <div title="./img/Schowek01.bmp"  img="img">obrazek</div>
        
        
        <div title="./img.php" post="{'jakaś zmienna' : 'jakaś wartość zmiennej'}">img z php</div>
        
      </div>

  </body>

</html>