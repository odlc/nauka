<!DOCTYPE HTML>
<html lang="pl">
<head>
  <meta charset="UTF-8">
  <title></title>
  <link rel="stylesheet" type="text/css" href="/css/css.css" media="all" />
  <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="js/test.js"></script>
  <style type="text/css"> /* layout boxy */
    body {
      background-color: lightblue;
    }
    #container {
      width: 940px;
      margin: auto;
      height: 1000px;
      border: 1px solid black;
      background-color: #ACCCE5;
    }
    #footer {
      width: 940px;
      height: 100px;
      margin: auto;
      border: 1px solid black;
      margin-top: 5px;
      background-color: #E6D056;
    }
    #header {
      width: 100%;
      height: 150px;
      border-bottom: 1px solid black;
    }
    .round {
      -webkit-border-radius: 3px; /* Safari, Chrome */
      -khtml-border-radius: 3px;    /* Konqueror */
      -moz-border-radius: 3px; /* Firefox */
      border-radius: 3px; /* IE 9 */
    }
    #left {
      float: left;
      width: 230px;
      border-right: 1px solid black;
    }
    #cont {
      float: right;
      width:  709px;
    }
    #left button {
      display: block;
      clear: both;
    }
    #header > h1 { /* elementy h1 znajdującę się BEZPOŚREDNIO w elemencie o id header */
      color: yellow;
    }
    #header  h1 { /* elementy h1 znajdującę się BEZPOŚREDNIO w elemencie o id header */
      border-bottom: 1px dotted red;
    }
    #header > * > h1 {
      color: cyan;
    }
    .example h1 {
      color: green;
    }
  </style>
  
  
  <style type="text/css"> <!-- selektory -->
    
    
  </style>
</head>
<body>
  
  <div id="container" class="round">
    
    <div id="header">
      <h1>Test Header</h1>
      <div>
        <h1>gdzieś głębiej</h1>
      </div>
    </div>
    <div id="left"">
      <h1>Test Header</h1>
      <button id="append">test</button>
    </div>
    <div id="cont" class="example">
      <h1>Test Header</h1>
      
      
      <div id="target">pusty</div>
      
      
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu mi eget nunc vehicula tempus. </p>
      <p>Suspendisse potenti. Vivamus ullamcorper massa ut risus faucibus tincidunt. Ut sodales porta bibendum. Pellentesque sagittis, mauris nec commodo faucibus, dolor leo faucibus leo, et aliquet odio nunc at tellus. Donec id eros orci. Nunc dolor mauris, accumsan condimentum laoreet eget, porttitor ut libero. In felis leo, faucibus non varius nec, sagittis elementum erat. Morbi non ultrices ligula. Ut malesuada sodales ipsum quis tincidunt. Sed sit amet purus sed sem venenatis molestie. In viverra gravida arcu non pellentesque. In hendrerit lacinia turpis vitae blandit. Nulla turpis metus, ultricies quis rhoncus non, rutrum et quam. Proin erat nisl, cursus iaculis ultrices a, imperdiet eget urna.Quisque mattis arcu non libero auctor quis accumsan felis facilisis. Integer porta dolor ac dui iaculis blandit. Sed vel odio orci. Etiam tortor erat, laoreet eget rhoncus in, euismod nec purus. Nulla facilisi. Praesent diam quam, placerat non varius eget, imperdiet id justo. Nulla eu massa lorem, quis egestas orci.Nunc non nibh ut lorem sodales mollis. Mauris justo purus, posuere sit amet commodo vitae, viverra nec quam. Nulla diam odio, sodales elementum lobortis sed, accumsan nec odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec a mi arcu. Phasellus ut nulla eros. Sed egestas urna ac nisl faucibus consectetur. Nulla ipsum ligula, ornare non mollis ac, laoreet vel odio. Etiam porttitor, ligula et ultricies lobortis, enim nisl dictum metus, et rutrum risus nibh fringilla justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce eu viverra libero. Nullam imperdiet laoreet elementum. Etiam lobortis imperdiet leo non vulputate. Phasellus id est eu mauris lacinia accumsan.In hac habitasse platea dictumst. Nam euismod mollis ante, id faucibus nisi ultricies sit amet. Cras sed nulla tellus, vel cursus leo. Integer gravida diam in lectus porta ut dapibus nisl venenatis. Vivamus volutpat felis a nibh aliquam eu eleifend libero convallis. Praesent odio lectus, consequat aliquam interdum eget, fringilla quis dolor. Cras elementum ultrices velit sit amet semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;Etiam pharetra iaculis purus at lacinia. Maecenas urna eros, accumsan et vulputate at, facilisis at justo. Suspendisse ipsum massa, interdum sit amet sollicitudin sed, fermentum quis mi. Fusce laoreet adipiscing neque eu dapibus. Cras neque ante, tempus eget pellentesque id, mollis id neque. Integer luctus lacus quis lorem semper posuere. Curabitur diam orci, ornare nec elementum et, malesuada sed nisl. Morbi eu ipsum ut sem congue hendrerit hendrerit nec mi. Sed eu augue et libero sollicitudin mattis. Fusce diam mauris, gravida tristique mattis a, rhoncus et lacus. Vestibulum consectetur rhoncus leo, ut vulputate diam porttitor sit amet. Vivamus iaculis molestie ultricies. </p>
    </div>
    
  </div>
  
  <div id="footer" class="round">
      <h1>Test Header</h1>
    <p>stopka</p>
  </div>
  <div id="temp" style="display: none;">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
  </div>
  
</body>
</html>